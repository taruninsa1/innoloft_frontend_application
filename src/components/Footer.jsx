import React from "react";
import styled from "styled-components";
import whiteLogo from "../logo_innoloft_white.png";
import {
  FaLinkedin,
  FaTwitter,
  FaYoutube,
  FaFacebookF,
  FaInstagram,
  FaPenNib,
} from "react-icons/fa";

// Outer container styling
const Container = styled.div`
  background-color: #272e71;
  color: #fff;
  padding-top: 5%;
  padding-bottom: 10%;
  width: 100%;
  position: absolute;
  z-index: 2;
`;

// inner container styling
const FooterBar = styled.div`
  padding-left: 12%;
  padding-right: 10%;
  display: grid;
  grid-template: 1fr 2fr 2fr/ 1fr 1fr;
  justify-items: left;
  column-gap: 10px;
  row-gap: 10px;
  .logo {
    grid-column: 1/3;
    padding-bottom: 25px;
    .whiteLogo {
      margin-left: -20px;
      width: 150px;
      height: 50px;
    }
  }
  .head {
    font-size: 0.9em;
    font-weight: 600;
  }
  a {
    color: #fff;
    text-decoration: none;
  }
  button {
    color: #fff;
    font-size: 16px;
    font-weight: 300;
    background-color: transparent;
    border: 0px;
    display: block;
    margin-top: 8px;
    margin-left: 10px;
    .icon {
      margin-right: 0.5em;
    }
  }
  .register {
    margin-left: 2px;
    margin-top: 5px;
    padding: 10px;
    background-color: #e4b302;
    border-radius: 4px;
  }

  /* For Bigger Screen */
  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-rows: auto;
    .logo {
      grid-column: 1/5;
    }
  }
`;

const Footer = () => {
  return (
    <Container>
      <FooterBar>
        <div className="logo">
          <img className="whiteLogo" src={whiteLogo} alt="Innoloft" />
        </div>
        <div className="social">
          <div className="head">Social Media</div>
          <button>
            <FaLinkedin className="icon" />
            LinkedIn
          </button>
          <button>
            <FaTwitter className="icon" />
            Twitter
          </button>
          <button>
            <FaFacebookF className="icon" />
            Facebook
          </button>
          <button>
            <FaInstagram className="icon" /> Instagram
          </button>
          <button>
            <FaYoutube className="icon" /> YouTube
          </button>
          <button>
            <FaPenNib className="icon" /> Blog
          </button>
        </div>
        <div className="legal">
          <div className="head">Legal Matters</div>
          <button>Terms of Usage</button>
          <button>Privacty Policies</button>
          <button>Imprint and Legal Noticie</button>
        </div>
        <div className="contact">
          <div className="head">Contact</div>
          <p>
            Innoloft GmbH
            <br />
            c/o DigitalHUB Aachen
            <br />
            Jülicher Straße 72a
            <br />
            52070 Aachen
            <br />
            <a href="mailto:info@innoloft.com">info@innoloft.com</a>
          </p>
        </div>
        <div className="Login">
          <div className="head">Login</div>
          <button className="register">Register for free</button>
        </div>
      </FooterBar>
    </Container>
  );
};

export default Footer;
