import React, { Component } from "react";
import { MemoryRouter as Router, Switch, Route, Link } from "react-router-dom";
import styled from "styled-components";
import { FaUserEdit, FaAddressBook } from "react-icons/fa";

// Importing form child components
import BasicData from "./Form/BasicData";
import Address from "./Form/Address";

// Styling for the custom designed Tab Container
const TabContainer = styled.div`
  margin-left: 60px;
  padding-top: 40px;
  padding-bottom: 20px;
  padding-right: 20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: stretch;
  text-align: center;
  a {
    text-decoration: none;
    color: initial;
  }
  .tabs {
    padding: 10px;
    border-radius: 1px;
    background-color: #ccc;
    border: 1px solid #777;
    font-size: 16px;
    .icon {
      font-size: 20px;
      padding: 5px;
      vertical-align: middle;
    }
  }
  .content {
    grid-column: 1/3;
    background-color: #eee;
    border: 1px solid #777;
    border-top: 0px;
  }
  .active {
    font-weight: 600;
    background-color: #eee;
    border-bottom: 0px solid #777;
    border-top: 2px solid #272e71;
    box-shadow: 0px -10px 10px -10px rgba(39, 46, 113, 1);
  }
`;

class Content extends Component {
  state = { firstTab: true };
  changeTab = (ft) => {
    this.setState({ firstTab: ft });
  };
  render() {
    return (
      <TabContainer>
        <Router>
          <Link to="/basic">
            <div
              className={this.state.firstTab ? "tabs active" : "tabs"}
              onClick={() => {
                this.changeTab(true);
              }}
            >
              <FaUserEdit className="icon" />
              Basic Data
            </div>
          </Link>
          <Link to="/address">
            <div
              className={this.state.firstTab ? "tabs" : "tabs active"}
              onClick={() => {
                this.changeTab(false);
              }}
            >
              <FaAddressBook className="icon" />
              Address
            </div>
          </Link>
          <div className="content">
            <Switch>
              <Route path="/address" component={Address} />
              <Route path="/basic" component={BasicData} />
              <Route path="/" component={BasicData} />
            </Switch>
          </div>
        </Router>
      </TabContainer>
    );
  }
}

export default Content;
