import React from "react";
import styled from "styled-components";
import { FaBars, FaGlobe, FaCaretDown, FaChevronDown } from "react-icons/fa";
import colorLogo from "../logo_innoloft.png";

// styling for main outer container
const Container = styled.div`
  width: 100%;
  position: sticky;
  top: 0;
  color: #323b49;
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.2);
  z-index: 200;
  background-color: #fff;
`;

// styling for inner container
const HeaderBar = styled.div`
  display: flex;
  align-items: center;
  background-color: #fff;
  max-width: 1155px;
  margin: 0 auto;
  height: 60px;
  color: #323b49;
  z-index: 20;
  button,
  .network,
  .product,
  .innoloft {
    background-color: transparent;
    color: #323b49;
    border: 0;
    font-size: 0.9rem;
    padding: 8px;
    * {
      margin: 2px;
      vertical-align: middle;
    }
  }
  .logoContainer {
    flex-grow: 1;
    .colorLogo {
      width: 120px;
      height: 40px;
    }
  }
  .menu {
    padding: 0.5rem;
    font-size: 1.2rem;
  }
  .items,
  .user {
    display: none;
    color: #323b49;
  }
  .register {
    color: #fff;
    background-color: #e4b302;
    border-radius: 4px;
  }

  .network,
  .product,
  .innoloft {
    position: relative;
    display: inline-block;
  }

  .networkContent,
  .productContent,
  .innoloftContent {
    display: none;
    position: absolute;
    background-color: white;
    min-width: 180px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  }

  .networkContent button {
    display: block;
    padding: 12px 16px;
  }
  .network:hover .networkContent {
    display: block;
  }

  .productContent button {
    display: block;
    padding: 12px 16px;
  }
  .product:hover .productContent {
    display: block;
  }

  .innoloftContent button {
    display: block;
    padding: 12px 16px;
  }
  .innoloft:hover .innoloftContent {
    display: block;
  }
  /* For Bigger screens  */
  @media (min-width: 768px) {
    height: 70px;
    .logoContainer {
      flex-grow: 0;
      .colorLogo {
        width: 210px;
        height: 70px;
      }
    }
    .menu {
      display: none;
    }
    .user {
      display: flex;
    }
    .items {
      display: flex;
      flex-grow: 1;
    }
    button {
      padding: 10px;
      * {
        margin: 4px;
      }
    }
  }
`;

const Header = () => {
  return (
    <Container>
      <HeaderBar>
        <div className="logoContainer">
          <img className="colorLogo" src={colorLogo} alt="Innoloft" />
        </div>
        <div className="items">
          <div className="list">
            <button>How it works</button>
            <div className="network">
              Network
              <FaChevronDown />
              <div className="networkContent">
                <button>Startups</button>
                <button>Requests</button>
                <button>News</button>
                <button>Challenges</button>
                <button>Speed-Datings</button>
              </div>
            </div>
            <div className="product">
              Products
              <FaChevronDown />
              <div className="productContent">
                <button>Use Cases</button>
                <button>Innoloft Pro</button>
                <button>Challenge</button>
                <button>Innovation Consulting</button>
                <button>Platforms</button>
                <button>Startup Speed-Dating</button>
              </div>
            </div>
            <div className="innoloft">
              Innoloft
              <FaChevronDown />
              <div className="innoloftContent">
                <button>Vision</button>
                <button>Team</button>
                <button>Career</button>
              </div>
            </div>
            <button>Blog</button>
          </div>
        </div>
        <div className="user">
          <button>Login</button>
          <button className="register">Register for free</button>
          <button>
            <FaGlobe />
            EN
            <FaCaretDown />
          </button>
        </div>
        <div className="menu">
          <FaBars />
        </div>
      </HeaderBar>
    </Container>
  );
};

export default Header;
