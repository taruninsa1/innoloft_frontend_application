import React from "react";
import styled from "styled-components";
import {
  FaHome,
  FaUserCircle,
  FaBuilding,
  FaCog,
  FaNewspaper,
  FaChartArea,
} from "react-icons/fa";

// left SideBar Stying
const SideBar = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  left: 0;
  z-index: 1;
  background: #fff;
  height: 100%;
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.3);
  .item {
    display: flex;
    align-items: center;
    padding: 10px;
    color: #555;
    border: 0px;
    font-size: 28px;
    cursor: pointer;
    .item-text {
      margin-left: 15px;
      margin-right: 15px;
      font-size: 16px;
      display: none;
    }
  }
  :hover .item-text {
    display: block;
  }
  .item:hover {
    color: #111;
    background-color: #ddd;
  }
`;

const Aside = () => {
  return (
    <SideBar>
      <div className="item">
        <FaHome />
        <div className="item-text">Home</div>
      </div>
      <div className="item">
        <FaUserCircle />
        <div className="item-text">My Account</div>
      </div>
      <div className="item">
        <FaBuilding />
        <div className="item-text">My Company</div>
      </div>
      <div className="item">
        <FaCog />
        <div className="item-text">My Settings</div>
      </div>
      <div className="item">
        <FaNewspaper />
        <div className="item-text">News</div>
      </div>
      <div className="item">
        <FaChartArea />
        <div className="item-text">Analytics</div>
      </div>
    </SideBar>
  );
};

export default Aside;
