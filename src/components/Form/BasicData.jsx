import React from "react";
import styled from "styled-components";
import { useFormik } from "formik";

// react notification stuff and animations
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import "animate.css";

//password strength estimator
const zxcvbn = require("zxcvbn");

// Styling for the custom made strength indicator
const Strength = styled.div`
  width: 100%;
  background-color: #ddd;
  border-radius: 2px;
  li {
    color: transparent;
  }
  .empty {
    background-color: red;
    width: 1%;
  }
  .red {
    background-color: red;
    width: 25%;
  }
  .yellow {
    background-color: yellow;
    width: 50%;
  }
  .green {
    background-color: green;
    width: 75%;
  }
  .full {
    background-color: green;
    width: 100%;
  }
`;

// Styling for the basic form
const Form = styled.form`
  display: grid;
  grid-template: auto / 1fr 1fr;
  max-width: 300px;
  row-gap: 10px;
  margin: 0 auto;
  padding: 10px;
  justify-items: left;
  * {
    width: 100%;
    border-radius: 2px;
  }
  .changeMail,
  .changePass {
    grid-column: 1/3;
  }
  .error {
    color: red;
    justify-self: center;
    grid-column: 1/3;
  }
  .submit {
    width: 80%;
    padding: 4px;
    justify-self: center;
    grid-column: 1/3;
  }
`;

// Validating user inputs
const validate = (values) => {
  // handles almost every email format
  const RegEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;

  const errors = {};

  if (!values.newEmail) {
    errors.newEmail = "Required";
  } else if (!RegEmail.test(values.newEmail)) {
    errors.newEmail = "Invalid email address";
  }

  if (!values.confirmEmail) {
    errors.confirmEmail = "Required";
  } else if (values.newEmail !== values.confirmEmail) {
    errors.confirmEmail = "Check Confirm Email Address";
  }

  if (!values.newEmail) {
    errors.newEmail = "Required";
  } else if (!RegEmail.test(values.newEmail)) {
    errors.newEmail = "Invalid email address";
  }

  if (!values.oldPass) {
    errors.oldPass = "Required";
  }

  if (!values.newPass) {
    errors.newPass = "Required";
  } else if (zxcvbn(values.newPass).score < 3) {
    errors.newPass = "Weak Password, Use symbols, letters and numbers";
  }

  if (!values.confirmPass) {
    errors.confirmPass = "Required";
  } else if (values.newPass !== values.confirmPass) {
    errors.confirmPass = "Password are not same";
  }

  return errors;
};

// handling submit event, fake call and notification
const onSubmit = (values, { resetForm }) => {
  fetch("https://httpbin.org/status/200").then((res) => {
    if (res.status === 200) {
      store.addNotification({
        title: "Success",
        message: "Your Data is saved successfully",
        type: "success",
        container: "top-right",
        animationIn: ["animated", "bounceInRight"],
        animationOut: ["animated", "bounceOutRight"],
        dismiss: {
          duration: 5000,
        },
      });
      resetForm({});
    }
  });
};

// change initial values here
const initialValues = {
  newEmail: "",
  confirmEmail: "",
  oldPass: "123@XYZabc",
  newPass: "",
  confirmPass: "",
};

// Basic Data Component
const BasicData = () => {
  // helper function to set colour and progress of strength indicator
  const checkPass = (pass) => {
    let score = zxcvbn(pass).score;
    document
      .getElementById("indicator")
      .classList.remove("empty", "red", "yellow", "green", "full");
    if (score === 0) {
      document.getElementById("indicator").classList.add("empty");
    } else if (score === 1) {
      document.getElementById("indicator").classList.add("red");
    } else if (score === 2) {
      document.getElementById("indicator").classList.add("yellow");
    } else if (score === 3) {
      document.getElementById("indicator").classList.add("green");
    } else if (score === 4) {
      document.getElementById("indicator").classList.add("full");
    }
  };

  // initialising formik
  const formik = useFormik({
    initialValues,
    validate,
    onSubmit,
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      <div className="changeMail">Change Email Address</div>
      <label htmlFor="newEmail">New Email</label>
      <input
        id="newEmail"
        type="text"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.newEmail}
      />
      <div className="error">
        {formik.touched.newEmail && formik.errors.newEmail}
      </div>
      <label htmlFor="confirmEmail">Confirm Email</label>
      <input
        id="confirmEmail"
        type="text"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.confirmEmail}
      />
      <div className="error">
        {formik.touched.confirmEmail && formik.errors.confirmEmail}
      </div>

      <div className="changePass">Change Password</div>

      <label htmlFor="oldPass">Old Password</label>
      <input
        id="oldPass"
        type="password"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.oldPass}
      />
      <div className="error">
        {formik.touched.oldPass && formik.errors.oldPass}
      </div>

      <label htmlFor="newPass">New Password</label>
      <input
        id="newPass"
        type="password"
        onBlur={formik.handleBlur}
        onChange={(e) => {
          checkPass(e.target.value);
          formik.handleChange(e);
        }}
        value={formik.values.newPass}
      />
      <div className="error">
        {formik.touched.newPass && formik.errors.newPass}
      </div>
      <label htmlFor="strength">Password Strength</label>
      <Strength>
        <li id="indicator" className="empty"></li>
      </Strength>
      <label htmlFor="confirmPass">Confirm Password</label>
      <input
        id="confirmPass"
        type="password"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.confirmPass}
      />
      <div className="error">
        {formik.touched.confirmPass && formik.errors.confirmPass}
      </div>

      <button type="submit" className="submit">
        Submit
      </button>
    </Form>
  );
};
export default BasicData;
